var gulp         = require('gulp');
var browserSync  = require('browser-sync').create();
var pug          = require('gulp-pug');
var sass         = require('gulp-sass');
var spritesmith  = require('gulp.spritesmith');
var rimraf       = require('rimraf');
var autoprefixer = require('gulp-autoprefixer');
var imagemin     = require('gulp-imagemin');
var del          = require('del');
//----------------Delete-------------------
gulp.task('clean', async function() {
    return del.sync('build');
});

//--------------Server--------------------------
gulp.task('server', function() {
    browserSync.init({
        server: {
            port: 9000,
            baseDir: "app"
        }
    });
    gulp.watch("app/sass/**/*.sass", gulp.series('sass'));
    gulp.watch('app/template/**/*.pug', gulp.series('template'));
    gulp.watch("app/*.html").on('change', browserSync.reload);
    gulp.watch("app/js/**/*.js").on('change', browserSync.reload);
});

//--------Pug complite---------------
gulp.task('template', function buildHTML() {
    return gulp.src('app/template/index.pug')
        .pipe(pug({
            pretty:true
        }))
        .pipe(gulp.dest('app'))
        .pipe(browserSync.stream());
});

//----------------Sass----------------------

gulp.task('sass', function() {
    return gulp.src("app/sass/*.sass")
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer(['last 5 versions', 'ie 8'], {cascade: true}))
        .pipe(gulp.dest("app/css"))
        .pipe(browserSync.stream());
});

//----------Sprites-----------------------


gulp.task('sprite', function () {
    var spriteData = gulp.src('app/img/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: 'app/img/sprite.png',
        cssName: 'sprite.sass'
    })
    );

     return spriteData.img.pipe(gulp.dest('app/img/global/')),
            spriteData.css.pipe(gulp.dest('app/sass/global/'));

});

//-------------Img mimifider-------------------

gulp.task('img', () =>
    gulp.src('app/img/**/*.*')
        .pipe(imagemin([
    imagemin.gifsicle({interlaced: true}),
    imagemin.jpegtran({progressive: true}),
    imagemin.optipng({optimizationLevel: 5}),
    imagemin.svgo({
        plugins: [
            {removeViewBox: true},
            {cleanupIDs: false}
        ]
    })
]))
        .pipe(gulp.dest('build/img'))
);


//------------Css Libs-------------------------

// gulp.task('css-libs', ['sass'], function() {
// 		return gulp.src('app/css/libs.css')
// 				.pipe(cssnano())
// 				.pipe(rename({suffix: '.min'}))
// 				.pipe(gulp.dest('app/css'));
// });
//--------------Copy-------------------------

gulp.task('copy', async function() {

        var buildCssmedia = gulp.src('app/css/main.css')
            .pipe(gulp.dest('build/css'));
          
        var buildFonts = gulp.src('app/fonts/**/*') 
            .pipe(gulp.dest('build/fonts'));

        var buildJs = gulp.src('app/js/*.js')
            .pipe(gulp.dest('build/js'));

        var buildHtml = gulp.src('app/*.html')
            .pipe(gulp.dest('build'));
});

//--------------Build-------------------------
gulp.task('build', gulp.series(
    gulp.parallel('clean', 'img', 'sass'),
    gulp.parallel('copy')
    ) 
    );

//--------------Derault---------------------
gulp.task('default', gulp.series(
    gulp.parallel('template', 'sass', 'sprite'),
    gulp.parallel('server')
));